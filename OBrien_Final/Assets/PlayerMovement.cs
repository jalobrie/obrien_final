﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController2D controller;

    public float runSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;

    bool haveCape = false;

    public bool haveShoot = false;
    bool isGrounded = false;

    public int jumpCount = 1;

    public int Health = 100;
   
    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            print("DID THIS!");
            jump = true;
            
        }

        if (Input.GetButton("Jump")&&(controller.m_Rigidbody2D.velocity.y < 0 && haveCape))
        {
            Debug.Log("this is working");
            Physics2D.gravity = new Vector3(0, -2.0F, 0);
        }
        else
        {
            Physics2D.gravity = new Vector3(0, -9.81F, 0);
        }

      
  

        


    }

    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Cape"))
        {
            Destroy(other.gameObject);
            haveCape = true;

            
        }
        else if (other.gameObject.CompareTag("DoubleJump"))
        {
            Destroy(other.gameObject);
            jumpCount = 2;

        }
        else if (other.gameObject.CompareTag("Shoot"))
        {
            Destroy(other.gameObject);
            haveShoot = true;
        }
        else if (other.gameObject.CompareTag("Spike"))
        {
            Health--;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag ("EndHouse"))
        {
            print("End Level");
        }
    }
}
